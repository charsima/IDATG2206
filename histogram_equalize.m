function equalized_img = histogram_equalize (img, hist)
    if ~exist('hist', 'var')
        hist = get_histogram(img);
    end

    [width, height] = size(img);

    for i = 2:256
       hist(1, i) = hist(1, i - 1) + hist(1, i); 
    end
    
    equalized_img = uint8(zeros(width, height));
    
    for x = 1:width
       for y = 1:height
           p = img(x, y);
           equalized_img(x, y) = hist(1, 1 + p) * 255;
       end
    end
end