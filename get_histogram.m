function hist = get_histogram (img)
    [width, height] = size(img);
    
    hist = zeros(1, 256);
    
    for x = 1:width
       for y = 1:height
          p = img(x, y) + 1;
          hist(1, p) = hist(1, p) + 1;
       end
    end
    
    hist = hist / (width * height); 
end