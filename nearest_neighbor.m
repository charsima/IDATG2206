function scaledImg = nearest_neighbor(img, targetResolution)
    [currentWidth, currentHeight] = size(img);
    targetWidth = targetResolution(1);
    targetHeight = targetResolution(2);
    
    scaledImg = uint8(zeros(targetWidth, targetHeight));
    xRatio = currentWidth / targetWidth;
    yRatio = currentHeight / targetHeight;
    
    for y = 0:1:targetHeight - 1
        for x = 0:1:targetWidth - 1
            px = floor(x * xRatio) + 1;
            py = floor(y * yRatio) + 1;
            
            scaledImg(x + 1, y + 1) = img(px, py);
        end
    end
end