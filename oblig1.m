img = imread('lena512.bmp');

% Task 1

nnImg = nearest_neighbor(img, size(img) * 4);
blImg = bilinear(img, size(img) * 4);
bcImg = bicubic(img, size(img) * 4);

imwrite(nnImg, "out/task1/nnImg.bmp");
imwrite(blImg, "out/task1/blImg.bmp");
imwrite(bcImg, "out/task1/bcImg.bmp");

% Task 2
imwrite(histogram_equalize(img), "out/task1/img_eq.bmp");
imwrite(histogram_equalize(nnImg), "out/task1/nnImg_eq.bmp");
imwrite(histogram_equalize(blImg), "out/task1/blImg_eq.bmp");
imwrite(histogram_equalize(bcImg), "out/task1/bcImg_eq.bmp");