function scaledImg = bicubic(img, targetResolution)
    [currentWidth, currentHeight] = size(img);
    targetWidth = targetResolution(1);
    targetHeight = targetResolution(2);
    
    xScale = targetWidth / currentWidth;
    yScale = targetHeight / currentHeight;
    
    scaledImg = uint8(zeros(targetWidth, targetHeight));
    img_pad = zeros(currentWidth + 4,currentHeight + 4);
    img_pad(2:currentWidth + 1, 2:currentHeight + 1, :) = img;
    img_pad = double(img_pad);

    for x = 1:targetWidth
        x1 = ceil(x / xScale); 
        x2 = x1 + 1; 
        x3 = x2 + 1;
        p = uint16(x1);

        if(xScale > 1)
           m1 = ceil(xScale * (x1 - 1));
           m2 = ceil(xScale * (x1));
           m3 = ceil(xScale * (x2));
           m4 = ceil(xScale * (x3));
        else
           m1 = (xScale * (x1 - 1));
           m2 = (xScale * (x1));
           m3 = (xScale * (x2));
           m4 = (xScale * (x3));
        end
        
        X = [ (x-m2)*(x-m3)*(x-m4)/((m1-m2)*(m1-m3)*(m1-m4)) ...
              (x-m1)*(x-m3)*(x-m4)/((m2-m1)*(m2-m3)*(m2-m4)) ...
              (x-m1)*(x-m2)*(x-m4)/((m3-m1)*(m3-m2)*(m3-m4)) ...
              (x-m1)*(x-m2)*(x-m3)/((m4-m1)*(m4-m2)*(m4-m3))];
          
        for y = 1:targetHeight
            y1 = ceil(y / yScale); 
            y2 = y1 + 1; 
            y3 = y2 + 1;
            
            if (yScale > 1)
               n1 = ceil(yScale * (y1 - 1));
               n2 = ceil(yScale * (y1));
               n3 = ceil(yScale * (y2));
               n4 = ceil(yScale * (y3));
            else
               n1 = (yScale * (y1 - 1));
               n2 = (yScale * (y1));
               n3 = (yScale * (y2));
               n4 = (yScale * (y3));
            end
            
            Y = [ (y-n2)*(y-n3)*(y-n4)/((n1-n2)*(n1-n3)*(n1-n4)); ...
                  (y-n1)*(y-n3)*(y-n4)/((n2-n1)*(n2-n3)*(n2-n4)); ...
                  (y-n1)*(y-n2)*(y-n4)/((n3-n1)*(n3-n2)*(n3-n4)); ...
                  (y-n1)*(y-n2)*(y-n3)/((n4-n1)*(n4-n2)*(n4-n3))];

            q = uint16(y1);
            sample = img_pad(p:p + 3,q:q + 3, :);
            scaledImg(x, y) = X * sample(:, :) * Y;
        end
    end
    
    scaledImg = uint8(scaledImg);
end