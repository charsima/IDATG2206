function scaledImg = bilinear(img, targetResolution)
    [currentWidth, currentHeight] = size(img);
    targetWidth = targetResolution(1);
    targetHeight = targetResolution(2);
    
    scaledImg = uint8(zeros(targetWidth, targetHeight));
    xRatio = (currentWidth - 1) / targetWidth;
    yRatio = (currentHeight - 1) / targetHeight;
    
    for y = 0:1:targetHeight - 1
        for x = 0:1:targetWidth - 1
            px = floor(xRatio * x);
            py = floor(yRatio * y);
            xDiff = (xRatio * x) - px;
            yDiff = (yRatio * y) - py;
            
            px = px + 1;
            py = py + 1;
            
            a = double(img(px, py));
            b = double(img(px + 1, py));
            c = double(img(px, py + 1));
            d = double(img(px + 1, py + 1));
            
            resultingPixel = a * (1 - xDiff) * (1 - yDiff);
            resultingPixel = resultingPixel + b * (xDiff) * (1 - yDiff);
            resultingPixel = resultingPixel + c * (1 - xDiff) * (yDiff);
            resultingPixel = resultingPixel + d * (xDiff) * (yDiff);
            
            scaledImg(x + 1, y + 1) = uint8(resultingPixel);
        end
    end
end